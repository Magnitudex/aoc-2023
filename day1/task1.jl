sum = 0
for word in collect(eachline("input"))
    a = collect(eachmatch(r"[0-9]", word))
    if size(a) == 1
        global sum += parse(Int64, a[1])
    else
        global sum += parse(Int64, a[1].match * last(a).match)
    end
end
 print(sum)
