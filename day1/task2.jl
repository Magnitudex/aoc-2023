sum = 0
sub_pairs = Dict("one" => "o1e", "two" => "t2o", "three" => "t3e", "four" => "f4r", "five" => "5e", "six" => "6", "seven" => "7n", "eight" => "e8t", "nine" => "9")
for word in collect(eachline("input"))
    for (key, value) in sub_pairs
        word = replace(word, key => value)
    end
    a = collect(eachmatch(r"[0-9]", word))
    if size(a) == 1
        global sum += parse(Int64, a[1])
    else
        global sum += parse(Int64, a[1].match * last(a).match)
    end
end
print(sum)
